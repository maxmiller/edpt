-- we don't know how to generate schema transparencia (class Schema) :(
create table folha_pagamento
(
	nome varchar(200) not null,
	salario double(10,2) not null,
	cargo varchar(200) not null,
	vinculo varchar(30) not null,
	ingresso varchar(4) not null,
	mes varchar(30) not null,
	ano varchar(4) not null
);
