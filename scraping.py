#http://186.226.7.26/transparencia/leiacessoinf.aspx
import time
from bs4 import BeautifulSoup
from utils import Servidor
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#import MySQLdb

def detalhar_salario(id_link, driver):

    try:
        time.sleep(0.150)
        botao = driver.find_element_by_id(id_link)
        botao.click()
        time.sleep(0.150)
        soup = BeautifulSoup(driver.page_source, "html.parser")
        table = soup.find("table", { "id" : "ctl00_ContentPlaceHolder1_dvRemuneracao" })
        rows =  table.findAll("tr")
        rvinculo = rows[1].findAll("td")
        vinculo = rvinculo[1].find(text=True)
        rexercicio = rows[2].findAll("td")
        exercicio = rexercicio[1].find(text=True)
        driver.execute_script("window.history.go(-1)");
        return vinculo,exercicio
    except:
        driver.execute_script("window.history.go(-1)");
        return "N/A", "N/A"


def buscar_salario_mes(indice_mes):
    lista = []
    #driver = webdriver.Remote( command_executor='http://127.0.0.1:4444/wd/hub',desired_capabilities=DesiredCapabilities.CHROME)

    driver = webdriver.Firefox()
    driver.get('http://186.226.7.26/transparencia/leiacessoinf.aspx')
    botao = driver.find_element_by_id('ctl00_ContentPlaceHolder1_btnPesquisar')

    mes = Select(driver.find_element_by_id('ctl00_ContentPlaceHolder1_ddlMesReferencia'))
    mes.select_by_index(indice_mes)


    botao.click()

    page = 2
    has_next_page = True
    while(has_next_page):
        time.sleep(0.500)
        soup = BeautifulSoup(driver.page_source, "html.parser")
        table = soup.find("table", { "id" : "ctl00_ContentPlaceHolder1_gvFuncionarios" })
        for row in table.findAll("tr"):
            cells = row.findAll("td")
            if len(cells) == 7:

                nome = cells[0].find()
                #print(nome)
                id = nome.get('id')
                orgao = cells[1].find(text=True)
                cargo = cells[2].find(text=True)
                carga_horaria = cells[3].find(text=True)
                remuneracao = cells[4].find()
                desconto = cells[5].find(text=True)
                rliquido = cells[6].find(text=True)
                vinculo,ingresso = detalhar_salario(id,driver)
                servidor = Servidor(nome.text,remuneracao.text,cargo,vinculo, ingresso)
                lista.append(servidor)
                time.sleep(0.500)
        has_next_page=False
        try:
            pagina = str(page)
            if( (page - 1) % 10 == 0):
                pagina = "..."
            if pagina == "...":
                for a in driver.find_elements_by_xpath('.//a'):
                    stLink = "Page$"+str(page)
                    if stLink in a.get_attribute('href'):
                        a.click()
                        break
            else:
                link = driver.find_element_by_link_text(str(pagina))
                link.click()
            #    print(link)
            page = page + 1
            has_next_page = True
        except NoSuchElementException:
            print("Sem links")

    driver.close()
    return lista

def gravar_arquivo_mes():
    meses = ["Outubro_2018","Setembro_2018","Agosto_2018","Junho_2018","Maio_2018","Abril_2018","Marco_2018","Fevereiro_2018","Janeiro_2018","Dezembro_2017","Novembro_2017","Outubro_2017","Setembro_2017","Agosto_2017","Julho_2017","Junho_2017","Maio_2017","Abril_2017","Marco_2017","Fevereiro_2017","Janeiro_2017"]
    intervalo = range(len(meses))
    for i in range(12,len(meses)):
        #conexao = MySQLdb.connect(host="localhost", user="root", passwd="123456", db="transparencia")
        print(meses[i])
        arquivo  = open(meses[i]+".csv","w")
        arquivo.write("Nome;Salario;Cargo;Vinculo;Ingresso;Mes;Ano\n")
        val = buscar_salario_mes(i)
        print("Total de servidores = ", len(val))
        for s in val:
            mes_ano = meses[i].split('_')
            mes = mes_ano[0]
            ano = mes_ano[1]
            #cursor = conexao.cursor()
            #cursor.execute("INSERT INTO FOLHA_PAGAMENTO(NOME, SALARIO,CARGO,VINCULO,INGRESSO,MES,ANO)",(s.nome,s.salario,s.cargo,s.vinculo,s.ingresso,mes,ano))
            arquivo.write(s.nome)
            arquivo.write(";")
            arquivo.write(s.salario)
            arquivo.write(";")
            arquivo.write(s.cargo)
            arquivo.write(";")
            arquivo.write(s.vinculo)
            arquivo.write(";")
            arquivo.write(s.ingresso)
            arquivo.write(";")
            arquivo.write(mes)
            arquivo.write(";")
            arquivo.write(ano)
            arquivo.write(";\n")


        arquivo.close()
        #conexao.commit()

        print(meses[i])

gravar_arquivo_mes()
