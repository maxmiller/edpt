#http://186.226.7.26/transparencia/leiacessoinf.aspx
import time
from bs4 import BeautifulSoup
from utils import Servidor
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

def buscar_salario_mes(indice_mes):
    lista = []
    driver = webdriver.Remote( command_executor='http://127.0.0.1:4444/wd/hub',desired_capabilities=DesiredCapabilities.FIREFOX)

    #driver = webdriver.Firefox()
    driver.get('http://186.226.7.26/transparencia/leiacessoinf.aspx')
    botao = driver.find_element_by_id('ctl00_ContentPlaceHolder1_btnPesquisar')

    mes = Select(driver.find_element_by_id('ctl00_ContentPlaceHolder1_ddlMesReferencia'))
    mes.select_by_index(indice_mes)


    botao.click()

    page = 2
    has_next_page = True
    while(has_next_page):
        time.sleep(1)
        soup = BeautifulSoup(driver.page_source, "html.parser")
        table = soup.find("table", { "id" : "ctl00_ContentPlaceHolder1_gvFuncionarios" })
        for row in table.findAll("tr"):
            cells = row.findAll("td")
            if len(cells) == 7:
                nome = cells[0].find()
                orgao = cells[1].find(text=True)
                cargo = cells[2].find(text=True)
                carga_horaria = cells[3].find(text=True)
                remuneracao = cells[4].find()
                desconto = cells[5].find(text=True)
                rliquido = cells[6].find(text=True)
                servidor = Servidor(nome.text,remuneracao.text,cargo)
                lista.append(servidor)
        has_next_page=False
        try:
            pagina = str(page)
            if( (page - 1) % 10 == 0):
                pagina = "..."
            if pagina == "...":
                for a in driver.find_elements_by_xpath('.//a'):
                    stLink = "Page$"+str(page)
                    if stLink in a.get_attribute('href'):
                        a.click()
                        break
            else:
                link = driver.find_element_by_link_text(str(pagina))
                link.click()
            #    print(link)
            page = page + 1
            has_next_page = True
        except NoSuchElementException:
            print("Não há mais links")

    driver.close()
    return lista

def gravar_arquivo_mes():
    meses = ["Setembro2018"]#,"Agosto2018","Junho2018","Maio2018","Abril2018","Marco2018","Fevereiro2018","Janeiro2018","Dezembro2017","Novembro2017","Outubro2017","Setembro2017","Agosto2017","Julho2017","Junho2017","Maio2017","Abril2017","Marco2017","Fevereiro2017","Janeiro2017"]
    for i in range(len(meses)):
        arquivo  = open(meses[i]+".csv","w")
        arquivo.write("Nome;Salario;Cargo;\n")
        for s in buscar_salario_mes(i):
            arquivo.write(s.nome)
            arquivo.write(";")
            arquivo.write(s.salario)
            arquivo.write(";")
            arquivo.write(s.cargo)
            arquivo.write(";\n")
        arquivo.close()

        print(meses[i])

gravar_arquivo_mes()
