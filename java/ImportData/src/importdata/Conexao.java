/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package importdata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author max
 */
public class Conexao {

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public Conexao() {

        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection("jdbc:mysql://localhost/transparencia?user=root&password=123456");
        } catch (SQLException e) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void inserir(Servidor s){
        
        try {
            
            preparedStatement = connect.prepareStatement("insert into  transparencia.folha_pagamento (nome, salario, cargo,vinculo,ingresso,mes,ano)values (?, ?, ?, ?, ? , ?, ?)");
            preparedStatement.setString(1, s.getNome());
            preparedStatement.setFloat(2, s.getSalario());
            preparedStatement.setString(3, s.getCargo());
            preparedStatement.setString(4, s.getVinculo());
            preparedStatement.setString(5, s.getIngresso());
            preparedStatement.setString(6, s.getMes());
            preparedStatement.setString(7, s.getAno());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void fechar(){
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
