/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package importdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author max
 */
public class ImportData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try {
            JFileChooser jfile = new JFileChooser(new File("/home/max/Projetos/jucurutu"));
            jfile.setMultiSelectionEnabled(true);
            jfile.showDialog(null, "Selecionar");
            File[] files = jfile.getSelectedFiles();
            FileReader fr;

            Conexao con = new Conexao();
            for (File file : files) {
                fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
                String s;

                //ENQUANTO EXISTE DADOS CONTINUA IMPRIMINDO
                int i = 1;

                s = br.readLine();
                
                while ((s = br.readLine()) != null) {
                    String[] valores = s.split(";");
                    float salario = Float.parseFloat(valores[1].replace(".", "").replace(",", "."));
                    String nome = valores[0];
                    String cargo = valores[2];
                    String vinculo = valores[3];
                    String ingresso = valores[4];
                    String mes = valores[5];
                    String ano = valores[6];
                    Servidor servidor = new Servidor(nome, vinculo, ano, mes, cargo, ingresso, salario);
                    con.inserir(servidor);
                    System.out.println("Já foi inserido " + i + " registros");
                    System.out.println("Mes:"+servidor.getMes());
                    i++;
                }
            }
            con.fechar();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportData.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
