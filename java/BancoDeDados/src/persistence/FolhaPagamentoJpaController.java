/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import entities.FolhaPagamento;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistence.exceptions.NonexistentEntityException;

/**
 *
 * @author max
 */
public class FolhaPagamentoJpaController implements Serializable {

    public FolhaPagamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("BancoDeDadosPU");
    }
    
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(FolhaPagamento folhaPagamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(folhaPagamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(FolhaPagamento folhaPagamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            folhaPagamento = em.merge(folhaPagamento);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = folhaPagamento.getId();
                if (findFolhaPagamento(id) == null) {
                    throw new NonexistentEntityException("The folhaPagamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FolhaPagamento folhaPagamento;
            try {
                folhaPagamento = em.getReference(FolhaPagamento.class, id);
                folhaPagamento.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The folhaPagamento with id " + id + " no longer exists.", enfe);
            }
            em.remove(folhaPagamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<FolhaPagamento> findFolhaPagamentoEntities() {
        return findFolhaPagamentoEntities(true, -1, -1);
    }

    public List<FolhaPagamento> findFolhaPagamentoEntities(int maxResults, int firstResult) {
        return findFolhaPagamentoEntities(false, maxResults, firstResult);
    }

    private List<FolhaPagamento> findFolhaPagamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FolhaPagamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FolhaPagamento findFolhaPagamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FolhaPagamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getFolhaPagamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FolhaPagamento> rt = cq.from(FolhaPagamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public FolhaPagamento findByNomeAnoMesSalario(FolhaPagamento f){
        return getEntityManager().createNamedQuery("FolhaPagamento.findByNomeAnoMesSalario",FolhaPagamento.class).setParameter("ano", f.getAno()).setParameter("mes", f.getMes()).setParameter("salario", f.getSalario()).setParameter("nome", f.getNome()).getSingleResult();
    }
    
}
