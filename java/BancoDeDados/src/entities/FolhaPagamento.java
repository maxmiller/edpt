/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author max
 */
@Entity
@Table(name = "folha_pagamento")
@NamedQueries({
    @NamedQuery(name = "FolhaPagamento.findAll", query = "SELECT f FROM FolhaPagamento f"),
    @NamedQuery(name = "FolhaPagamento.findByNome", query = "SELECT f FROM FolhaPagamento f WHERE f.nome = :nome"),
    @NamedQuery(name = "FolhaPagamento.findBySalario", query = "SELECT f FROM FolhaPagamento f WHERE f.salario = :salario"),
    @NamedQuery(name = "FolhaPagamento.findByCargo", query = "SELECT f FROM FolhaPagamento f WHERE f.cargo = :cargo"),
    @NamedQuery(name = "FolhaPagamento.findByVinculo", query = "SELECT f FROM FolhaPagamento f WHERE f.vinculo = :vinculo"),
    @NamedQuery(name = "FolhaPagamento.findByIngresso", query = "SELECT f FROM FolhaPagamento f WHERE f.ingresso = :ingresso"),
    @NamedQuery(name = "FolhaPagamento.findByMes", query = "SELECT f FROM FolhaPagamento f WHERE f.mes = :mes"),
    @NamedQuery(name = "FolhaPagamento.findByAno", query = "SELECT f FROM FolhaPagamento f WHERE f.ano = :ano"),
    @NamedQuery(name = "FolhaPagamento.findByNomeAnoMesSalario", query = "SELECT f FROM FolhaPagamento f WHERE f.ano = :ano and f.mes = :mes and f.nome = :nome and f.salario = :salario"),
    @NamedQuery(name = "FolhaPagamento.findById", query = "SELECT f FROM FolhaPagamento f WHERE f.id = :id")})
public class FolhaPagamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "salario")
    private double salario;
    @Basic(optional = false)
    @Column(name = "cargo")
    private String cargo;
    @Basic(optional = false)
    @Column(name = "vinculo")
    private String vinculo;
    @Basic(optional = false)
    @Column(name = "ingresso")
    private String ingresso;
    @Basic(optional = false)
    @Column(name = "mes")
    private String mes;
    @Basic(optional = false)
    @Column(name = "ano")
    private String ano;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public FolhaPagamento() {
    }

    public FolhaPagamento(Integer id) {
        this.id = id;
    }

    public FolhaPagamento(Integer id, String nome, double salario, String cargo, String vinculo, String ingresso, String mes, String ano) {
        this.id = id;
        this.nome = nome;
        this.salario = salario;
        this.cargo = cargo;
        this.vinculo = vinculo;
        this.ingresso = ingresso;
        this.mes = mes;
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public String getIngresso() {
        return ingresso;
    }

    public void setIngresso(String ingresso) {
        this.ingresso = ingresso;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FolhaPagamento)) {
            return false;
        }
        FolhaPagamento other = (FolhaPagamento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ID:" + id + " \n"
                + "Nome:"+nome+"\n"
                + "Mes:"+mes+"\n"
                + "Ano:"+ano+"\n"
                ;
    }
    
}
