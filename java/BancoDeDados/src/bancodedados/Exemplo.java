/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancodedados;

/**
 *
 * @author max
 */
public class Exemplo {
    
    private Exemplo() {
    }
    
    public static Exemplo getInstance() {
        return ExemploHolder.INSTANCE;
    }
    
    private static class ExemploHolder {

        private static final Exemplo INSTANCE = new Exemplo();
    }
}
